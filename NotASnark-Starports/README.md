# Starports Asset Pack for Dungeondraft

Author: Samuel Penn (sam@notasnark.net)

License: CC-BY-SA

This is a set of SciFi assets for use with Dungeondraft.
They are designed for use with the Traveller RPG, but should be useful for
any SciFi maps, especially starports and buildings.

For the source files for these assets, see:
https://gitlab.com/samuelpenn/starports-dungeondraft

